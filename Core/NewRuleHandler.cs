﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class NewRuleHandler
    {
        ISaveRuleCommand saveCmd;

        public NewRuleHandler(ISaveRuleCommand saveCmd)
        {
            this.saveCmd = saveCmd;
        }

        public void Handle(Rule rule)
        {
            saveCmd.SaveRule(rule);
        }
    }
}
