﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class Rule
    {
        public Rule(Guid id, string description, bool isActive = true)
        {
            Id = id;
            Description = description;
            IsActive = isActive;
        }

        public Guid Id { get; private set; }
        public string Description { get; private set; }
        public bool IsActive { get; set; }
    }
}
