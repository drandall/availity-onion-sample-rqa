﻿using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class RuleResultBatch
    {
        public RuleResultBatch(Organization organization, IEnumerable<RuleResult> results, DateTimeOffset start, DateTimeOffset end)
        {
            Organization = organization;
            Results = results;
            Start = start;
            End = end;
        }

        public Guid? Id { get; set; }
        public Organization Organization { get; private set; }
        public IEnumerable<RuleResult> Results { get; private set; }
        public DateTimeOffset Start { get; private set; }
        public DateTimeOffset End { get; private set; }
    }
}
