﻿using Availity.PatientAccess.Rqa.Encounters;
using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class RuleService
    {
        IRuleEngine ruleEngine;
        IRuleBatchSaveCommand resultsSaveCmd;
        IOrganizationByIdQuery orgQuery;
        IRulesQuery rulesQuery;

        public RuleService(IOrganizationByIdQuery orgQuery, IRulesQuery rulesQuery, IRuleEngine ruleEngine, IRuleBatchSaveCommand resultsSaveCmd)
        {
            this.ruleEngine = ruleEngine;
            this.rulesQuery = rulesQuery;
            this.resultsSaveCmd = resultsSaveCmd;
            this.orgQuery = orgQuery;
        }

        public RuleResultBatch RunRules(Encounter encounter, Guid organizationId)
        {
            var startTime = DateTimeOffset.UtcNow;
            var organization = orgQuery.GetOrganizationById(organizationId);
            var activeRules = rulesQuery.GetRules();
            var rulesToRun = activeRules.Where(r => organization.RuleIdsInUse.Contains(r.Id)).Select(r => r.Id).ToList();
            var results = ruleEngine.Run(rulesToRun, encounter);
            var batch = new RuleResultBatch(organization, results, startTime, DateTimeOffset.UtcNow);
            resultsSaveCmd.SaveBatch(batch);
            return batch;
        }
    }
}
