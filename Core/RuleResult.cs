﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class RuleResult
    {
        public RuleResult(Guid ruleId, string message)
        {
            RuleId = ruleId;
            Message = message;
        }

        public Guid RuleId { get; private set; }
        public string Message { get; private set; }
        public bool HasError { get { return !string.IsNullOrEmpty(Message); } }
        public string Severity { get { return HasError ? "Error" : "Clean"; } }
    }
}
