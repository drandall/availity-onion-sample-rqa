﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.Organizations
{
    public interface ISaveOrganizationCommand
    {
        void SaveOrganization(Organization organization);
    }
}
