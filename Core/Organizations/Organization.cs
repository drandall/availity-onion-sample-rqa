﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.Organizations
{
    public class Organization
    {
        HashSet<Guid> ruleIds;

        public Organization(Guid id, IEnumerable<Guid> ruleIdsInUse)
        {
            Id = id;
            ruleIds = new HashSet<Guid>(ruleIdsInUse ?? Enumerable.Empty<Guid>());
        }

        public Guid Id { get; private set; }

        public IEnumerable<Guid> RuleIdsInUse { get { return ruleIds.AsEnumerable(); } }

        public void UseRule(Rule rule)
        {
            ruleIds.Add(rule.Id);
        }

        public void DontUseRule(Rule rule)
        {
            ruleIds.Remove(rule.Id);
        }
    }
}
