﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.Organizations
{
    public class NewOrganizationHandler
    {
        IRulesQuery rulesQuery;
        ISaveOrganizationCommand saveCmd;

        public NewOrganizationHandler(IRulesQuery rulesQuery, ISaveOrganizationCommand saveCmd)
        {
            this.rulesQuery = rulesQuery;
            this.saveCmd = saveCmd;
        }

        public void Handle(Guid id)
        {
            var activeRules = rulesQuery.GetRules();
            var org = new Organization(id, activeRules.Select(r => r.Id));
            saveCmd.SaveOrganization(org);
        }
    }
}
