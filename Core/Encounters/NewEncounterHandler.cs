﻿using Availity.PatientAccess.Rqa.Encounters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.Encounters
{
    public class NewEncounterHandler
    {
        RuleService ruleService;

        public NewEncounterHandler(RuleService ruleService)
        {
            this.ruleService = ruleService;
        }

        public RuleResultBatch Handle(Encounter encounter, Guid organizationId)
        {
            return ruleService.RunRules(encounter, organizationId);
        }
    }
}
