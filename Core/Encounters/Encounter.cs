﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.Encounters
{
    public class Encounter
    {
        public Encounter(Patient patient)
        {
            Patient = patient;
        }

        public Patient Patient { get; private set; }
    }
}
