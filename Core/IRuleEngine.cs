﻿using Availity.PatientAccess.Rqa.Encounters;
using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public interface IRuleEngine
    {
        IEnumerable<RuleResult> Run(IEnumerable<Guid> ruleIds, Encounter encounter);
    }
}
