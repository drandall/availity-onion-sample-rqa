﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class RuleRemovedHandler
    {
        IRuleByIdQuery ruleQuery;
        ISaveRuleCommand saveCmd;

        public RuleRemovedHandler(IRuleByIdQuery ruleQuery, ISaveRuleCommand saveCmd)
        {
            this.ruleQuery = ruleQuery;
            this.saveCmd = saveCmd;
        }

        public void RemoveRule(Guid id)
        {
            var rule = ruleQuery.GetRuleById(id);

            if (rule == null)
                throw new ArgumentException("id");

            rule.IsActive = false;

            saveCmd.SaveRule(rule);
        }
    }
}
