﻿using Autofac;
using Availity.PatientAccess.Rqa;
using Availity.PatientAccess.Rqa.Encounters;
using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa.EventListener
{
    public class Startup
    {
        static IContainer container;
        static Random randomNumberGenerator = new Random();

        public static void Main(string[] args)
        {
            try
            {
                InitContainer();

                while (true)
                {
                    PrintMenu();

                    var key = Console.ReadKey();

                    Console.Clear();

                    switch (key.Key)
                    {
                        case ConsoleKey.Q:
                            return;
                        case ConsoleKey.E:
                            NewEncounter();
                            break;
                        case ConsoleKey.C:
                            if ((key.Modifiers & ConsoleModifiers.Shift) != 0)
                            {
                                DeleteDatabase();
                            }
                            break;
                        case ConsoleKey.R:
                            if ((key.Modifiers & ConsoleModifiers.Shift) != 0)
                            {
                                RemoveRandomRule();
                            }
                            else
                            {
                                NewRule();
                            }
                            break;
                        case ConsoleKey.O:
                            NewOrganization();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine("Any key to exit");
                Console.ReadKey();
            }
        }

        private static void InitContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<NewEncounterHandler>();
            builder.RegisterType<NewRuleHandler>();
            builder.RegisterType<NewOrganizationHandler>();
            builder.RegisterType<RuleRemovedHandler>();
            builder.RegisterType<RuleService>();
            AceStartup.RegisterImplementations(builder);
            DataStartup.RegisterImplementations(builder);
            container = builder.Build();
        }

        private static void PrintMenu()
        {
            PrintBar();
            Console.WriteLine("\tq\tQuit");
            Console.WriteLine("\te\tSend in a new Encounter");
            Console.WriteLine("\tShift+c\tClear your database and start fresh");
            Console.WriteLine("\tr\tAdd a new Rule to the system");
            Console.WriteLine("\tShift+r\tRemove a random Rule from the system");
            Console.WriteLine("\to\tAdd a new Organization to the system");
            PrintBar();
        }

        private static void PrintBar()
        {
            Console.WriteLine("============================================================");
        }

        private static void NewEncounter()
        {
            var handler = container.Resolve<NewEncounterHandler>();
            var organizations = container.Resolve<IOrganizationsQuery>().GetAllOrganizations().ToList();

            if (organizations.Count == 0)
            {
                Console.WriteLine("Add organization first");
                return;
            }

            var org = organizations[randomNumberGenerator.Next(organizations.Count)];
            var batch = handler.Handle(new Encounter(new Patient("test")), org.Id);
            Console.WriteLine($"{batch.Id.ToString()} ran in {(batch.End - batch.Start).TotalMilliseconds}ms and produced {batch.Results.Count()} results");
        }

        private static void NewRule()
        {
            var handler = container.Resolve<NewRuleHandler>();
            var id = Guid.NewGuid();
            handler.Handle(new Rule(id, $"Rule {id.ToString()}"));
            Console.WriteLine($"Rule {id.ToString()} added. New organizations will use this rule, older ones won't.");
        }

        private static void NewOrganization()
        {
            var handler = container.Resolve<NewOrganizationHandler>();
            var id = Guid.NewGuid();
            handler.Handle(id);
            Console.WriteLine($"Organization {id.ToString()} added. It is using all the current rules.");
        }

        private static void DeleteDatabase()
        {
            // this breaks a lot of rules, but c'mon, it's just a sample app
            File.Delete(Database.GetFileName());
            Console.WriteLine("Database deleted");
        }

        private static void RemoveRandomRule()
        {
            var availableRules =
                container
                    .Resolve<IRulesQuery>()
                    .GetRules()
                    .ToList();

            var rule = availableRules[randomNumberGenerator.Next(availableRules.Count)];

            container.Resolve<RuleRemovedHandler>().RemoveRule(rule.Id);
            Console.WriteLine($"Rule {rule.Id.ToString()} removed. Will no longer be returned in batches");
        }
    }
}
