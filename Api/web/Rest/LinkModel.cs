﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Availity.PatientAccess.Rqa.Rest
{
    public class LinkModel
    {
        public LinkModel(string desciption, string url)
        {
            Description = desciption;
            Url = url;
        }

        public string Description { get; private set; }
        public string Url { get; private set; }
    }
}