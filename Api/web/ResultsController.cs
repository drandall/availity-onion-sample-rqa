﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Availity.PatientAccess.Rqa
{
    [RoutePrefix("result-batches/{batchId}/results")]
    public class ResultsController : ApiController
    {
        IBatchDetailQuery query;

        public ResultsController(IBatchDetailQuery query)
        {
            this.query = query;
        }

        public const string GetResultsByBatchIdRouteName = "GetResultsByBatchId";
        [Route(Name = GetResultsByBatchIdRouteName)]
        [HttpGet]
        public IHttpActionResult GetResultsByBatchId(Guid batchId)
        {
            var urlHelper = Request.GetUrlHelper();
            return Ok(query.GetBatchById(batchId).Results.Select(r => new RuleResultModel(r, urlHelper)));
        }
    }
}