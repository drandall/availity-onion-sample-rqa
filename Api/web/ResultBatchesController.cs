﻿using Availity.PatientAccess.Rqa.Encounters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Availity.PatientAccess.Rqa
{
    [RoutePrefix("result-batches")]
    public class ResultBatchesController : ApiController
    {
        IAllBatchesQuery query;
        RuleService ruleService;

        public ResultBatchesController(IAllBatchesQuery query, RuleService ruleService)
        {
            this.query = query;
            this.ruleService = ruleService;
        }

        public const string GetResultBatchesRouteName = "GetAllResultBatches";
        [HttpGet]
        [Route(Name = GetResultBatchesRouteName)]
        public IHttpActionResult GetAllResultBatches()
        {
            var urlHelper = Request.GetUrlHelper();
            return Ok(query.GetAllBatches().Select(b => new ResultBatchSummaryModel(b, urlHelper)));
        }

        public const string GetNewResultBatchRouteName = "GetNewResultBatch";
        [HttpGet]
        [Route(template: "new", Name = GetNewResultBatchRouteName)]
        public IHttpActionResult GetNewResultBatch(Guid organizationId)
        {
            var urlHelper = Request.GetUrlHelper();
            var encounter = new Encounter(new Patient("web test"));
            var batch = ruleService.RunRules(encounter, organizationId);
            return Ok(new ResultBatchSummaryModel(batch, urlHelper));
        }
    }
}