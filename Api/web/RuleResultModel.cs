﻿using Availity.PatientAccess.Rqa.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Availity.PatientAccess.Rqa
{
    public class RuleResultModel
    {
        RuleResult core;
        UrlHelper urlHelper;

        public RuleResultModel(RuleResult core, UrlHelper urlHelper)
        {
            this.core = core;
            this.urlHelper = urlHelper;
        }

        public string Message { get { return core.Message; } }
        public string Severity { get { return core.Severity; } }

        public IEnumerable<LinkModel> Links
        {
            get
            {
                return new[]
                {
                    new LinkModel("all batches", urlHelper.Route(ResultBatchesController.GetResultBatchesRouteName, null))
                };
            }
        }
    }
}