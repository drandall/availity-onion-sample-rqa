﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Availity.PatientAccess.Rqa.Organizations
{
    [RoutePrefix("organizations")]
    public class OrganizationsController : ApiController
    {
        IOrganizationsQuery orgQuery;

        public OrganizationsController(IOrganizationsQuery orgQuery)
        {
            this.orgQuery = orgQuery;
        }

        public const string GetAllOrganizationsRouteName = "GetAllOrganizations";
        [HttpGet]
        [Route(Name = GetAllOrganizationsRouteName)]
        public IHttpActionResult GetAllOrganizations()
        {
            var urlHelper = Request.GetUrlHelper();
            return Ok(orgQuery.GetAllOrganizations().Select(o => new OrganizationModel(o, urlHelper)));
        }

        public const string GetOrganizationByIdRouteName = "GetOrganizationById";
        [HttpGet]
        [Route(template: "{id}", Name = GetOrganizationByIdRouteName)]
        public IHttpActionResult GetOrganizationById(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
