﻿using Availity.PatientAccess.Rqa.Organizations;
using Availity.PatientAccess.Rqa.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Availity.PatientAccess.Rqa
{
    public class ResultBatchSummaryModel
    {
        RuleResultBatch core;
        UrlHelper urlHelper;

        public ResultBatchSummaryModel(RuleResultBatch core, UrlHelper urlHelper)
        {
            this.core = core;
            this.urlHelper = urlHelper;
        }

        public DateTimeOffset StartTime { get { return core.Start; } }
        public DateTimeOffset EndTimeTime { get { return core.End; } }
        public object Counts
        {
            get
            {
                return new
                {
                    Rules = core.Results.Count(),
                    Errors = core.Results.Count(r => r.HasError),
                    Clean = core.Results.Count(r => !r.HasError)
                };
            }
        }

        public IEnumerable<LinkModel> Links
        {
            get
            {
                return new[]
                {
                    new LinkModel("organization details", urlHelper.Route(OrganizationsController.GetOrganizationByIdRouteName, new { id = core.Organization.Id.ToString() })),
                    new LinkModel("batch details", urlHelper.Route(ResultsController.GetResultsByBatchIdRouteName, new { batchId = core.Id.Value.ToString() }))
                };
            }
        }
    }
}