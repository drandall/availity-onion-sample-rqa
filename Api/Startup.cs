﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Availity.PatientAccess.Rqa
{
    public static class Startup
    {
        public static void RegisterImplementations(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            DataStartup.RegisterImplementations(builder);
            builder.RegisterType<RuleService>();
            AceStartup.RegisterImplementations(builder);
            DataStartup.RegisterImplementations(builder);
        }
    }
}