﻿using Availity.PatientAccess.Rqa.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;

namespace Availity.PatientAccess.Rqa.Organizations
{
    public class OrganizationModel
    {
        Organization core;
        UrlHelper urlHelper;

        public OrganizationModel(Organization core, UrlHelper urlHelper)
        {
            this.core = core;
            this.urlHelper = urlHelper;
        }

        public string Id { get { return core.Id.ToString(); } }

        public IEnumerable<Guid> RulesInUse { get { return core.RuleIdsInUse; } }

        public IEnumerable<LinkModel> Links
        {
            get
            {
                return new []
                {
                    new LinkModel("run rules now", urlHelper.Link(ResultBatchesController.GetNewResultBatchRouteName, new { organizationId = core.Id.ToString() }))
                };
            }
        }
    }
}