﻿using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Availity.PatientAccess.Rqa.Rest
{
    public class HomeController : ApiController
    {
        public const string DefaultRouteName = "Default";
        [Route("", Name = DefaultRouteName)]
        public IHttpActionResult GetBaseLinks()
        {
            var urlHelper = Request.GetUrlHelper();

            var links = new List<LinkModel>()
            {
                new LinkModel("result-batches", urlHelper.Link(ResultBatchesController.GetResultBatchesRouteName, null)),
                new LinkModel("organizations", urlHelper.Link(OrganizationsController.GetAllOrganizationsRouteName, null)),
                new LinkModel("rules", urlHelper.Link(RulesController.GetAllRulesRouteName, null))
            };

            return Ok(links);
        }
    }
}
