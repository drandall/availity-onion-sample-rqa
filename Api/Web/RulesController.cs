﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Availity.PatientAccess.Rqa
{
    [RoutePrefix("rules")]
    public class RulesController : ApiController
    {
        IRulesQuery query;

        public RulesController(IRulesQuery query)
        {
            this.query = query;
        }

        public const string GetAllRulesRouteName = "GetAllRules";
        [HttpGet]
        [Route(Name = GetAllRulesRouteName)]
        public IHttpActionResult GetAllRules()
        {
            return Ok(query.GetRules(activeOnly: false));
        }
    }
}
