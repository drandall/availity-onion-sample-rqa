﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.AceClient
{
    public class RuleResult
    {
        public Guid RuleId { get; set; }
        public string Message { get; set; }
    }
}
