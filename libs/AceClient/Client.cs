﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.AceClient
{
    public class Client
    {
        string[] errors =
            {
                "",
                "Patient sounds like a sissy",
                "Patient smells like Onions",
                "I know it says you were supposed to send PatientName as a string, but I actually wanted a byte array of frogs, because I'm Ace and I can do that"
            };

        public IEnumerable<RuleResult> PretendToDoAceThings(string patientName, IEnumerable<Guid> ruleIds)
        {
            var results = new List<RuleResult>();
            var rng = new Random();

            foreach (var ruleId in ruleIds)
            {
                results.Add(new RuleResult()
                {
                    Message = errors[rng.Next(errors.Length)],
                    RuleId = ruleId
                });
            }

            return results;
        }
    }
}
