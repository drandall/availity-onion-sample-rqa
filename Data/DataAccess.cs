﻿using Availity.PatientAccess.Rqa.Organizations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class DataAccess : IOrganizationByIdQuery, IOrganizationsQuery, ISaveOrganizationCommand, IAllBatchesQuery, IBatchDetailQuery, IRuleBatchSaveCommand, IRuleByIdQuery, IRulesQuery, ISaveRuleCommand
    {
        Database database = Database.GetDatabase();

        Organization IOrganizationByIdQuery.GetOrganizationById(Guid id)
        {
            Organization organization = null;
            database.OrganizationsById.TryGetValue(id, out organization);
            return organization;
        }

        IEnumerable<Organization> IOrganizationsQuery.GetAllOrganizations()
        {
            return database.OrganizationsById.Values;
        }

        void ISaveOrganizationCommand.SaveOrganization(Organization organization)
        {
            if (organization.RuleIdsInUse.Any(i => !database.RulesById.ContainsKey(i)))
                throw new ArgumentException("ruleId");

            database.OrganizationsById[organization.Id] = organization;
            database.SaveState();
        }

        IEnumerable<RuleResultBatch> IAllBatchesQuery.GetAllBatches()
        {
            return database.BatchesById.Values;
        }

        RuleResultBatch IBatchDetailQuery.GetBatchById(Guid id)
        {
            RuleResultBatch batch = null;
            database.BatchesById.TryGetValue(id, out batch);
            return batch;
        }

        void IRuleBatchSaveCommand.SaveBatch(RuleResultBatch resultBatch)
        {
            if (!database.OrganizationsById.ContainsKey(resultBatch.Organization.Id))
                throw new ArgumentException("organizationId");

            if (resultBatch.Results.Any(r => !database.RulesById.ContainsKey(r.RuleId)))
                throw new ArgumentException("ruleId");
            
            resultBatch.Id = Guid.NewGuid();
            database.BatchesById[resultBatch.Id.Value] = resultBatch;
            database.SaveState();
        }

        Rule IRuleByIdQuery.GetRuleById(Guid id)
        {
            Rule rule = null;
            database.RulesById.TryGetValue(id, out rule);
            return rule;
        }

        IEnumerable<Rule> IRulesQuery.GetRules(bool activeOnly)
        {
            return database.RulesById.Values.Where(r => !activeOnly || r.IsActive == true).ToList();
        }

        void ISaveRuleCommand.SaveRule(Rule rule)
        {
            database.RulesById[rule.Id] = rule;
            database.SaveState();
        }
    }
}
