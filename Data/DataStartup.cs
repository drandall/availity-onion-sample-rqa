﻿using Autofac;
using Availity.PatientAccess.Rqa.Organizations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public static class DataStartup
    {
        public static void RegisterImplementations(ContainerBuilder builder)
        {
            builder.RegisterType<DataAccess>()
                .As<IOrganizationByIdQuery>()
                .As<IOrganizationsQuery>()
                .As<ISaveOrganizationCommand>()
                .As<IAllBatchesQuery>()
                .As<IBatchDetailQuery>()
                .As<IRuleBatchSaveCommand>()
                .As<IRuleByIdQuery>()
                .As<IRulesQuery>()
                .As<ISaveRuleCommand>();
        }
    }
}
