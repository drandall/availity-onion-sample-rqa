﻿using Availity.PatientAccess.Rqa.Organizations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public class Database
    {
        public Dictionary<Guid, RuleResultBatch> BatchesById { get; set; } = new Dictionary<Guid, RuleResultBatch>();
        public Dictionary<Guid, Rule> RulesById { get; set; } = new Dictionary<Guid, Rule>();
        public Dictionary<Guid, Organization> OrganizationsById { get; set; } = new Dictionary<Guid, Organization>();

        public void SaveState()
        {
            using (var stream = File.Create(GetFileName()))
            using (var writer = new StreamWriter(stream))
            {
                var json = JsonConvert.SerializeObject(this);
                writer.Write(json);
            }
        }

        public static string GetFileName()
        {
            var dir = Environment.ExpandEnvironmentVariables(@"%localappdata%\Onion-Sample");
            Directory.CreateDirectory(dir);
            return Path.Combine(dir, "database");
        }

        public static Database GetDatabase()
        {
            Database database = null;

            var fileName = GetFileName();
            if (File.Exists(fileName))
            {
                var text = File.ReadAllText(fileName);

                try
                {
                    database = JsonConvert.DeserializeObject<Database>(text);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException($"Database has been corrupted. Please fix or delete {fileName} and try again", ex);
                }
            }

            if (database == null)
                return database = new Database();

            return database;
        }
    }
}
