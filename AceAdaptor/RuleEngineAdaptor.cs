﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Availity.PatientAccess.Rqa.Encounters;
using Availity.PatientAccess.Rqa.Organizations;

namespace Availity.PatientAccess.Rqa
{
    public class RuleEngineAdaptor : IRuleEngine
    {
        public IEnumerable<RuleResult> Run(IEnumerable<Guid> ruleIds, Encounter encounter)
        {
            var aceClient = new AceClient.Client();

            var aceResults = aceClient.PretendToDoAceThings(encounter.Patient.Name, ruleIds);
            var results = new List<RuleResult>();

            foreach (var aceResult in aceResults)
            {
                var result = new RuleResult(aceResult.RuleId, aceResult.Message);
                results.Add(result);
            }

            var ruleIdsWithoutResults = ruleIds.Except(aceResults.Select(r => r.RuleId));
            foreach (var ruleId in ruleIdsWithoutResults)
            {
                var result = new RuleResult(ruleId, null);
                results.Add(result);
            }

            return results;
        }
    }
}