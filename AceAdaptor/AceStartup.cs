﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Availity.PatientAccess.Rqa
{
    public static class AceStartup
    {
        public static void RegisterImplementations(ContainerBuilder builder)
        {
            builder.RegisterType<RuleEngineAdaptor>().As<IRuleEngine>();
        }
    }
}
